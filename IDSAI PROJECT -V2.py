import pandas as pd
import numpy as np
import seaborn as sb

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

data = pd.read_excel('WHR2018Chapter2OnlineData.xls', sheet_name="Table2.1")
data.head(20)

dataCol = []
for i in data:
    dataCol.append(i)
    if(i == 'Delivery Quality'):
        break
dataCol.append('GINI index (World Bank estimate), average 2000-15')
print(dataCol)


dataV1 = pd.DataFrame(data[dataCol])
dataV1.describe()


dataV2 = dataV1[dataV1.year != 2005]

dataV2.head(20)

dataMean = dataV2.groupby('country').mean().reset_index()
dataMean.head()

dataV3 = dataV2.merge(dataMean, on='country')

dataV3.shape

for i in range(1535):
    for j in range(2,15):
         if (np.isnan(dataV3.iloc[i,j])):
                dataV3.iloc[i,j] = dataV3.iloc[i,j+14]


dataV3.head()

dataV4 = dataV3.fillna(0)

dataV4.head()


finalDataCol = []
for i in dataV4:
    finalDataCol.append(i)
    if(i == 'GINI index (World Bank estimate), average 2000-15_x'):
        break
        
print(finalDataCol)


dataFinal = pd.DataFrame(dataV4[finalDataCol])

dataFinal.columns = [dataCol]

dataFinal.head(20)

dataRegion = pd.read_excel("WHR2018Chapter2OnlineData.xls", sheet_name="SupportingFactors")
dataRegion.head()


dataFinal.shape



dataR = pd.read_excel('WHR2018Chapter2OnlineData.xls', sheet_name="SupportingFactors")

dataRegion = pd.DataFrame(dataR, columns=['country','Region indicator'])

dataRegion.head()



regionsCol = []
i=0;
for names in dataRegion.country:
    for items in dataFinal.country.items():
        for places in items[1]:
            if (places==names and i!=1535):
                regionsCol.append(dataRegion['Region indicator'][i])
        i+=1
print(regionsCol)

print(len(regionsCol))
